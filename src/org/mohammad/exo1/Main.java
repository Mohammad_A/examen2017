package org.mohammad.exo1;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.mohammad.exo1.model.Movie;
import java.util.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main 
{
	public static void main(String[] args)
	{
		Movie movie = new Movie();
		
		// Q6
		Comparator<Movie> compareMovieYear = Comparator.comparing(m -> m.getReleaseYear());
		Comparator<Movie> compareMovieTitle = compareMovieYear.thenComparing(m -> m.getTitle());
		
		Movie m1 = (Movie) movie.of();
		Movie m2 = (Movie) movie.of();
		Movie m3 = (Movie) movie.of();
		
		m1.setTitle("Lush"); m1.setReleaseYear(1999);
		m2.setTitle("The Harvest"); m2.setReleaseYear(1993);
		m3.setTitle("Macbeth in Manhattan"); m3.setReleaseYear(1999);
		
		System.out.println("Q6/ Test du Comparator de deux instances de Movie : ");
		System.out.println("Comparons m1 et m2 : " + compareMovieTitle.compare(m1, m2));
		System.out.println("Comparons m1 et m3 : " + compareMovieTitle.compare(m1, m3));
		
		System.out.println("");
		
		List<String> fichier = listOfLines();
		
		// Q7 A
		String exemple = "8 Crazy Nights (2002)/Sandler, Adam/Sprouse, Cole/Sprouse, Dylan/Stout, Austin/Titone, Jackie";
		System.out.println("Q7/ A/ Test de la m�thode lineToTileAndYear() : ");
		System.out.println(exemple + " : " + movie.lineToTitleAndYear(exemple));
		
		System.out.println("");
		
		// Q7 B
		System.out.println("Q7/ B/ Test de la m�thode lineToYear() : ");
		System.out.println(exemple + " : " + movie.lineToYear(exemple));
		
		System.out.println("");
		
		// Q7 C
		System.out.println("Q7/ C/ Test de la m�thode lineToTitle() : ");
		System.out.println(exemple + " : " + movie.lineToTitle(exemple));
		
		System.out.println("");
		
		// Q8
		Function<String, Stream<Character>> charStream = s -> s.chars().mapToObj(i -> (char) i);
		long numberOfCharacters = 0;
		
		for(int i = 0; i < fichier.size(); i++)
		{
			charStream.apply(fichier.get(i));
			numberOfCharacters = numberOfCharacters + charStream.apply(fichier.get(i)).count();
		}
		System.out.println("Q8/ Stream de caract�re : ");
		System.out.println("--> Il y a " + numberOfCharacters + " caract�res dans ce stream ! ");
			
		System.out.println("");
		
		// Q9
		List<String> listMovie = new ArrayList<>();
		listMovie = fichier.stream().map(l -> movie.lineToTitleAndYear(l)).collect(Collectors.toList());
		
		//listMovie.stream().map(l -> movie.lineToYear(l)).forEach(y -> System.out.println(y));
		long nbrFilm = listMovie.stream().count();
		System.out.println("Q9/ Liste des films cr�e : ");
		System.out.println("--> Il y a " + nbrFilm + " film dans la liste !");
		
	}
	
	public static List<String> listOfLines ()
	{
		Path path = Paths.get("src/org/mohammad/exo1/movies-mpaa.txt");
		try (Stream<String> lines = Files.lines(path))
		{
			return lines.collect(Collectors.toList());
		} 
		catch (IOException e)
		{
			System.out.println("e.getMessage() = " + e.getMessage());
		}
		
		return null;
	}
}
