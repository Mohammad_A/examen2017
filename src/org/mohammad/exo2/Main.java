package org.mohammad.exo2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.mohammad.exo2.model.Actor;

public class Main 
{
	public static void main(String[] args)
	{
		// Q1
		Actor actor = new Actor();
		
		// Q2
		String exemple = "8 Crazy Nights (2002)/Sandler, Adam/Sprouse, Cole/Sprouse, Dylan/Stout, Austin/Titone, Jackie";
		
		System.out.println("Q2/ Test de la m�thode lineToNames() : ");
		System.out.println(exemple + " : " + actor.lineToNames(exemple));
		
		System.out.println("");
		
		// Q3
		System.out.println("Q3/ Test de la m�thode lineToActorLines() : " + actor.lineToNames(exemple));
		actor.lineToActorLines(actor.lineToNames(exemple));
		
		System.out.println("");
		
		// Q4
		Actor a1 = actor.of("Sandler, Adam");
		System.out.println("Q4/ Cr�ation d'une instance d'un acteur : Sandler, Adam");
		System.out.println(a1);
		
		System.out.println("");
		
		// Q5
		
		
		// Q7
		Comparator<Actor> compareByLastName = Comparator.comparing(p -> p.getLastName());
		Comparator<Actor> compareThenByFirstName = compareByLastName.thenComparing(p -> p.getFirstName());
		
		Actor a2 = actor.of("Messi, Lionel");
		Actor a3 = actor.of("Luis, Suarez");
		Actor a4 = actor.of("Messi, Pierre");
		
		System.out.println("Q7/ Test du comparateur : ");
		System.out.println("Messi Lionel / Luis Suarez : " + compareThenByFirstName.compare(a2, a3));
		System.out.println("Messi Lionel / Messi Pierre : " + compareThenByFirstName.compare(a2, a4));
		
		System.out.println("");
		
	}
	
	public static List<String> listOfLines ()
	{
		Path path = Paths.get("src/org/mohammad/exo1/movies-mpaa.txt");
		try (Stream<String> lines = Files.lines(path))
		{
			return lines.collect(Collectors.toList());
		} 
		catch (IOException e)
		{
			System.out.println("e.getMessage() = " + e.getMessage());
		}
		
		return null;
	}
	
}
