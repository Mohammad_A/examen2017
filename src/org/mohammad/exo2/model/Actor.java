package org.mohammad.exo2.model;

import java.io.Serializable;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Actor implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String lastName;
	private String firstName;
	
	public Actor()
	{
		
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String toString() {
		return "Actor [lastName=" + lastName + ", firstName=" + firstName + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Actor other = (Actor) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		return true;
	}
	
	public String lineToNames(String line)
	{
		Function<String, String> getNames = s -> s.substring(s.indexOf("/") + 1);
		return getNames.apply(line);
	}
	
	public Stream<String> lineToActorLines(String line)
	{
		BiFunction<String, String, Stream<String>> splitWordWithPattern = (l, pattern) -> Pattern.compile("[" + pattern + "]").splitAsStream(l);
		splitWordWithPattern.apply(line, "/").forEach(actor -> System.out.println(actor));
		return splitWordWithPattern.apply(line, "/");
	}
	
	
	public Actor of(String actorLine)
	{
		Class<?> cls;
		Actor o = null;
		
		try 
		{
			cls = Class.forName("org.mohammad.exo2.model.Actor");
			try 
			{
				o = (Actor) cls.newInstance();
				o.lastName = actorLine.split(",")[0];
				o.firstName = actorLine.split(",")[1];
			} 
			catch (InstantiationException e) 
			{
				System.out.println("Erreur : " + e.getMessage());
				e.printStackTrace();
			} 
			catch (IllegalAccessException e) 
			{
				System.out.println("Erreur : " + e.getMessage());
				e.printStackTrace();
			}
			
			return o;
		} 
		catch (ClassNotFoundException e) 
		{
			System.out.println("Erreur :" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
}