package org.mohammad.exo2.model;

import java.io.Serializable;
import java.util.function.Function;
import java.util.*;

public class Movie implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String title;
	private int releaseYear;
	
	public Movie()
	{
		
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}

	@Override
	public String toString() {
		return "Movie [title='" + title + "', releaseYear=" + releaseYear + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + releaseYear;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		if (releaseYear != other.releaseYear)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
	public Object of()
	{
		Class<?> cls;
		Object o = null;
		
		try 
		{
			cls = Class.forName("org.mohammad.exo1.model.Movie");
			try 
			{
				o = cls.newInstance();
			} 
			catch (InstantiationException e) 
			{
				System.out.println("Erreur : " + e.getMessage());
				e.printStackTrace();
			} 
			catch (IllegalAccessException e) 
			{
				System.out.println("Erreur : " + e.getMessage());
				e.printStackTrace();
			}
			
			return o;
		} 
		catch (ClassNotFoundException e) 
		{
			System.out.println("Erreur :" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public String lineToTitleAndYear(String line)
	{
		Function<String, String> getTitleAndYear = s -> s.split("/") [0];
		return getTitleAndYear.apply(line);
	}
	
	public int lineToYear(String line)
	{
		Function<String, Integer> getYear = s -> Integer.parseInt(s.substring(s.indexOf("(") + 1, s.indexOf(")")));
		lineToTitleAndYear(line);
		return getYear.apply(line);
	}
	
	public String lineToTitle(String line)
	{
		Function<String, String> getTitle = s -> s.substring(0, s.indexOf("(") - 1);
		lineToTitleAndYear(line);
		return getTitle.apply(line);
	}
	
	@SuppressWarnings("unused")
	private Collection<Actor> collectionActor = new ArrayList<Actor>();
}